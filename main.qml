import QtQuick 2.0
import QtCharts 2.0
import QtQuick.Controls 2.15
import QtWebSockets 1.15

// run: qmlscene main.qml

ApplicationWindow {
    visible: true

    property var period: 0.2
    property var numSamples: 50

    WebSocket {
        id: ws
        active: true
        url: "ws://localhost:8000/graph"
        onStatusChanged: (status) => {
            console.log(status + ws.errorString)
        }

        onTextMessageReceived: (message) => {
            const json = JSON.parse(message)
            if (lineSeries.count == 0) {
                lineSeries.append(0, json.value);
            } else {
                lineSeries.append(lineSeries.at(lineSeries.count - 1).x + period, json.value)
                xAxis.min = xAxis.min + period
                xAxis.max = xAxis.max + period
            }
            if (lineSeries.count > numSamples) {
                lineSeries.remove(0)
            }
            view.setAxisY(yAxis, lineSeries)
            view.setAxisX(xAxis, lineSeries)
        }
    }

    ChartView {
        title: "Oscillator"
        anchors.fill: parent
        antialiasing: true
        id: view

        ValueAxis {
            id: yAxis

            min: -20
            max: 20
        }

        ValueAxis {
            id: xAxis

            min: -1 * period * numSamples
            max: 0
        }

        LineSeries {
            name: "Cosine, WebSocket powered"
            id: lineSeries
        }
    }
}
