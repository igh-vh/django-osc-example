from django.urls import re_path, path
from . import consumers

websocket_urlpatterns = [
    path('control', consumers.WebsocketEndpoint.as_asgi()),
    path('graph', consumers.GraphEndpoint.as_asgi()),
]

channels = {
    "pdcom": consumers.PdComConsumer.as_asgi(),
}
