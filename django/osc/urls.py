from django.urls import path
from . import views

urlpatterns = [
   path("control", views.ControlView, name="control"),
   path("graph", views.GraphView, name="graph"),
   path("", views.HomeView, name="home"),
]
