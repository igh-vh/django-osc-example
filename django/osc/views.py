from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Create your views here.
def HomeView(request):
    context = {}
    return render(request,template_name="osc/home.html",context=context)

@login_required
def ControlView(request):
    context = {}
    return render(request,template_name="osc/control.html",context=context)

def GraphView(request):
    context = {}
    return render(request,template_name="osc/graph.html",context=context)
