from channels.generic.websocket import AsyncJsonWebsocketConsumer
from channels.consumer import AsyncConsumer
import asyncio
import pdcom5


class WebsocketEndpoint(AsyncJsonWebsocketConsumer):
    async def connect(self):
        # reject not authenticated users
        if self.scope["user"].is_anonymous:
            await self.close()
            return
        await self.accept()
        await self.channel_layer.group_add("osc", self.channel_name)
        await self.channel_layer.send("pdcom", {"type": "init.task"})

    async def receive_json(self, content):
        if "command" in content and content["command"] == "set_enable":
            await self.channel_layer.send(
                    "pdcom",
                    {
                        "type": "osc.enable",
                        "value": content.get("value", None)
                    },
                    )

    async def disconnect(self, code):
        await self.channel_layer.group_discard("osc", self.channel_name)

    async def newValues(self, event):
        await self.send_json({"value": event["value"]})


class GraphEndpoint(AsyncJsonWebsocketConsumer):
    async def connect(self):
        await self.accept()
        await self.channel_layer.group_add("osc", self.channel_name)
        await self.channel_layer.send("pdcom", {"type": "init.task"})
    async def newValues(self, event):
        await self.send_json({"value": event["value"]})


class PdComConsumer(AsyncConsumer):
    async def init_task(self, event):
        if hasattr(self, "_task"):
            return
        self._enable_var = None
        async def task():
            p = pdcom5.Process()
            await p.connect("msr://osc")
            self._enable_var = await p.find("/osc/enable")
            sub = await p.subscribe(0.2, "/osc/cos")
            while True:
                await self.channel_layer.group_send(
                        "osc",
                        {
                            "type": "newValues",
                            "value":(await sub.read())[0],
                        }
                )
        self._task = asyncio.create_task(task())

    async def osc_enable(self, content):
        if self._enable_var is not None and content.get("value", None) is not None:
            await self._enable_var.setValue(bool(content["value"]))
