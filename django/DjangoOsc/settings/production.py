from .base import *   # noqa: F401 F403
import os

ALLOWED_HOSTS = [os.environ.get("DJANGO_SERVERNAME", "localhost")]
# needs to be a full URL
CSRF_TRUSTED_ORIGINS = ["http://" + ALLOWED_HOSTS[0] + ":8000"]

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("DJANGO_SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!

# Database
# https://docs.djangoproject.com/en/4.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('POSTGRES_DB'),
        'USER': os.environ.get('DJANGO_PG_USER'),
        'PASSWORD': os.environ.get('DJANGO_PG_PASSWORD'),
        'HOST': os.environ.get('DJANGO_PG_HOST'),
        'PORT': '5432',
    }
}
