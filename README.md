# Django Oscillator Interface

This is a tiny demo which shows how to use live process data
in a web interface.
The cosine output of the [PdServ example](https://gitlab.com/etherlab.org/pdserv) is displayed.
The oscillator can be enabled and disabled using two buttons.

## How to run

You'll need docker and docker-compose.
Simply hit `docker-compose up` and navigate your favorite browser
to http://localhost:8000.

You'll have to set a superuser password,
execute the following command after the containers have been started:
```
docker-compose exec web python manage.py createsuperuser
```
It will ask for details, like your email address and a password.

Furthermore, you can connect the [Testmanager](https://gitlab.com/etherlab.org/testmanager) to the oscillator on msr://localhost:2345.

## Production Mode

Copy [example.env](example.env) to `.env` and change the secrets.
Startup all containers with `docker-compose -f docker-compose.production.yaml up`. Then change the password as explained above.

If the website should be reachable from the public,
adapt `DJANGO_SERVERNAME`.

## Behind the curtain

We use four containers:
 - osc: contains the pdserv oscillator
 - migrate: runs db migrations before starting the webserver and the worker
 - web: hosts the website itself
 - worker: Django Channels worker which connects to the oscillator
 - redis: Manages communication between web and worker

## Websockets

The frontend uses one WebSocket to talk to the backend,
an [`WebsocketEndpoint`](django/osc/consumers.py) instance.
This instance connects itself into the `osc`
[Channel Layer group](https://channels.readthedocs.io/en/stable/topics/channel_layers.html#groups).
The [JavaScript Button click handlers](django/osc/templates/osc/control.html)
send JSON-encoded commands to the `WebsocketEndpoint` instance.

The named Consumer `pdcom` is executed in the worker container
and broadcasts the cosine value to the `osc` group.
Also, it has a method to enable resp. disable the oscillator,
`osc.enable`.


## Implement in your own app

I used the following packages:

```
pip install channels["daphne"] channels_redis django django-channels pdcom5
```

And followed more or less [this tutorial](https://earthly.dev/blog/build-real-time-comm-app/).
Most of the party plays in [control.html](django/osc/templates/osc/control.html) and [consumers.py](django/osc/consumers.py).

Daphne is required to make the WebSockets work,
otherwise the routing in [asgi.py](django/DjangoOsc/asgi.py) is ignored.
